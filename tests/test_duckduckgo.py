"""
My first tests that I wrote in Python.

They verify that the website's welcome page works correctly.
"""
from selenium import webdriver
# from selenium.webdriver.common.keys import Keys

URL = "https://duckduckgo.com/"

browser = webdriver.Firefox()
browser.get(URL)
browser.maximize_window()

assert browser.title.startswith("DuckDuckGo")

search_box = browser.find_element_by_id("search_form_input_homepage")
search_box.send_keys("Ferienpass\n")
# search_box.send_keys(Keys.RETURN)

browser.save_screenshot("screenshot-duckduckgo.png")

browser.close()
browser.quit()
