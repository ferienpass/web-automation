"""
My first tests that I wrote in Python.

They verify that the website's welcome page works correctly.
"""
from selenium import webdriver

NEEDLE = "Ferienpass"
URL = f"https://www.google.ch/search?q={NEEDLE}"

browser = webdriver.Firefox()
browser.get(URL)

assert browser.title.startswith(NEEDLE)
assert "Google" in browser.title

browser.save_screenshot("screenshot-google.png")

browser.close()
browser.quit()
