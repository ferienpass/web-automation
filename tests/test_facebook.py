"""
My first tests that I wrote in Python.

They verify that the website's welcome page works correctly.
"""
from selenium import webdriver

list_of_accounts = [
    ('aaa@bbb.com', "lkjsdlkjdslkfj"),
    ('john@bbb.com', "lkjsdlkjdslkfj"),
    ('jane@bbb.com', "lkjsdlkjdslkfj"),
    ('jon.irgendwas@gmail.com', "sdlkfjslkdjf"),
    ('ada@bbb.com', "lkjsdlkjdslkfj"),
    ('axa@bbb.com', "lkjsdlkjdslkfj"),
]

for email, passwd in list_of_accounts:
    browser = webdriver.Firefox()
    browser.get("http://www.facebook.com")
    browser.implicitly_wait(5)  # seconds

    accept_button = browser.find_element_by_css_selector("button[data-cookiebanner=accept_button]")
    accept_button.click()

    username = browser.find_element_by_id("email")
    password = browser.find_element_by_id("pass")
    submit = browser.find_element_by_css_selector("button[name=login]")

    username.send_keys(email)
    password.send_keys(passwd + "\n")
    submit.click()

    # browser.save_screenshot("screenshot-facebook.png")
    # assert "Facebook" in browser.title

    browser.close()
    browser.quit()
