"""
My first test that I wrote in Python.

They verify that the website's welcome page works correctly.
"""
from pathlib import Path
from selenium import webdriver

# this way our example will work everywhere
public_folder = Path(__file__).resolve().parent.parent / 'public'

browser = webdriver.Firefox()
browser.maximize_window()
browser.get(f"file://{public_folder}/index.html")

document_caption = browser.find_element_by_css_selector('body > h1').text

assert browser.title == "Ferienpass 2020", \
    f"Unexpected document title: '{browser.title}'"
assert document_caption == "Ferienpass", \
    f"Main caption should be 'Ferienpass', found '{document_caption}'"

browser.close()
browser.quit()
