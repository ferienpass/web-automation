Ferienpass 2020
===============

My first homepage. My first tests. My first Python code.

Prerequisites
-------------

- Ubuntu 20.04
- Install via terminal:

  ```console
  sudo apt install pipenv
  sudo apt install default-jre-headless
  ```

HTML (My first homepage)
------------------------

```console
pipenv install html5validator
pipenv shell
```

Python (My first tests)
-----------------------

```console
pipenv install selenium
pipenv install flake8
pipenv install pytest
```
```console
sudo apt install chromium-chromedriver
sudo apt install firefox-geckodriver
```

Resources
---------

Learn HTML and Python:

- https://www.w3schools.com/html/html_basic.asp
- https://pythonbasics.org/

Validator tools:

- https://validator.w3.org/

Communication:

- https://ferienpass.slack.com
